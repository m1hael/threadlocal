#
# Thread Local Storage
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the program objects.
BIN_LIB=SHARED

#
# User-defined part end
#-------------------------------------------------------------------------------


all: env compile bind

env:

compile:
	$(MAKE) -C src/ compile $*

bind:
	$(MAKE) -C src/ bind $*
	
clean:
	$(MAKE) -C src/ clean $*

purge:
	$(MAKE) -C src/ purge $*

.PHONY:
