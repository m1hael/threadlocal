**FREE


/include 'threadlocal/threadlcl.rpginc'
/include 'noxDB/jsonxml.rpgle'


main();
*inlr = *on;


dcl-proc main;
  dcl-s tls pointer;
  dcl-s name varchar(100);
  dcl-s text char(50);
  
  tls = tls_getStorage();
  
  name = 'Mihael Schmidt';
  jx_setStr(tls : '/user/name' : name);
  
  text = jx_getStr(tls : '/user/name' : '');
  dsply text;  // Output: Mihael Schmidt
  
  text = jx_asJsonText(tls);
  dsply text;  // Output: {"user":{"name":"Mihael Schmidt"}}
end-proc;